# Starter Swarm Template - CFML <!-- omit in toc -->

The goal of this project is to provide a basic template for beginning to use and understand Docker Swarm in the context of a CFML application. It provides a pattern and structure for working with:

- Matching Local and Production development environments
- Use of a private registry
- CI/CD pipeline for building and deploying to your Swarm
- Management of sensitive information via secrets
- An approach to managing/adding/rotating secrets
- Adding Extensions (Lucee CFML specific)

Container orchestration in production requires a lot more than this (certs, load balancers, monitoring, etc), but these framework should provide a foundation for understanding and working with Docker Swarm in your ColdFusion application.

## Table of Contents <!-- omit in toc -->
- [Prerequisites](#prerequisites)
- [Project Steps](#project-steps)

## Prerequisites

Before getting started with this project, there are a few things that you'll need to have in place.

- **Gitlab account**: We'll host the repository with Gitlab, so that we can use their integrated container registry and CI/CD pipeline. It's really nice to have everything in one place. It's even nicer when it's free. [Register for Gitlab here](https://gitlab.com/users/sign_in#register-pane).
- **DigitalOcean account**: We need somewhere to host and deploy our Swarm. While the code here should work in the context of any cloud provider, I'll be using DigitalOcean in my instructions and examples. [Create a DigitalOcean account here](https://m.do.co/c/8acbd6928587) ($100 referral credit).
- **Docker for Mac/Windows**: Along with local development, we'll leverage the functionality added in Docker 18.09 to manage our remote Docker hosts from our local machine. [Get Docker Desktop here](https://www.docker.com/products/docker-desktop).

## Project Steps

1. Clone this repository, and navigate into it.
   ```
   git clone git@gitlab.com:mjclemente/starter-swarm-cfml.git
   cd starter-swarm-cfml
   ```
2. You'll need to create a Swarm. If you're new to this process, the easiest way to do this will be the [script here](https://github.com/mjclemente/do-swarm-create)